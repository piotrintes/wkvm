# sudo pip3 install keyboard
# sudo pip3 install evdev
# sudo dnf install python-evdev

import socket
import keyboard
import sys
from evdev import UInput, ecodes as e

HOST = sys.argv[1]      # The server's hostname or IP address
# HOST = "192.168.2.104"  # The server's hostname or IP address
PORT = 3001             # The port used by the server

# Create a virtual mouse device
ui = UInput({
    e.EV_KEY: [e.BTN_LEFT, e.BTN_RIGHT, e.BTN_MIDDLE, e.BTN_SIDE, e.BTN_EXTRA],
    e.EV_REL: [e.REL_X, e.REL_Y, e.REL_HWHEEL, e.REL_WHEEL],
}, name="VirtualMouse")

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    # s.sendall(b"Hello, world")
    data = s.recv(1024)
    print(f"Received {data!r}")
    device = ""
    while True:
        char = ""
        data = ""
        while char != "\n":
            data = data + char
            char = s.recv(1).decode()

        try:
            dev_ = data.split(":")[0][-1]
            event = data.split(":")[1]
            value = int(data.split(":")[2])
            device = dev_
        except:
            try:
                event = data.split(":")[0]
                value = int(data.split(":")[1])
            except:
                print("parse err")

        if device == "k":
            try:
                if event == "kp":
                    keyboard.press(value)
                elif event == "kr":
                    keyboard.release(value)
            except:
                print("keyboard err")
        elif device == "m":
            try:
                if event == "mh":
                    ui.write(e.EV_REL, e.REL_X, value)
                    ui.syn()

                elif event == "mv":
                    ui.write(e.EV_REL, e.REL_Y, value)
                    ui.syn()

                elif event == 'kp':
                    if value == 272:
                        ui.write(e.EV_KEY, e.BTN_LEFT, 1)
                        ui.syn()
                    elif value == 273:
                        ui.write(e.EV_KEY, e.BTN_RIGHT, 1)
                        ui.syn()
                    elif value == 274:
                        ui.write(e.EV_KEY, e.BTN_MIDDLE, 1)
                        ui.syn()
                    elif value == 275:
                        ui.write(e.EV_KEY, e.BTN_SIDE, 1)
                        ui.syn()
                    elif value == 276:
                        ui.write(e.EV_KEY, e.BTN_EXTRA, 1)
                        ui.syn()

                elif event == 'kr':
                    if value == 272:
                        ui.write(e.EV_KEY, e.BTN_LEFT, 0)
                        ui.syn()
                    elif value == 273:
                        ui.write(e.EV_KEY, e.BTN_RIGHT, 0)
                        ui.syn()
                    elif value == 274:
                        ui.write(e.EV_KEY, e.BTN_MIDDLE, 0)
                        ui.syn()
                    elif value == 275:
                        ui.write(e.EV_KEY, e.BTN_SIDE, 0)
                        ui.syn()
                    elif value == 276:
                        ui.write(e.EV_KEY, e.BTN_EXTRA, 0)
                        ui.syn()

                elif event == 'sv':
                    ui.write(e.EV_REL, e.REL_WHEEL, int(value/2))
                    ui.syn()
                elif event == 'sh':
                    ui.write(e.EV_REL, e.REL_HWHEEL, int(value/2))
                    ui.syn()
                else:
                    print(event, value)

            except:
                print("mouse err")
