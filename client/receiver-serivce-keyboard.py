# pip3 install keyboard
import socket
import keyboard

HOST = sys.argv[1]      # The server's hostname or IP address
# HOST = "192.168.2.104"  # The server's hostname or IP address
PORT = 3001             # The port used by the server

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    # s.sendall(b"Hello, world")
    data = s.recv(1024)
    print(f"Received {data!r}")
    device = ""
    while True:
        char = ""
        data = ""
        while char != "\n":
            data = data + char
            char = s.recv(1).decode()

        try:
            dev_ = data.split(":")[0][-1]
            event = data.split(":")[1]
            value = data.split(":")[2]
            device = dev_
        except:
            event = data.split(":")[0]
            value = data.split(":")[1]

        if device == "k":
            try:
                if event == "kp":
                    keyboard.press(int(value))
                elif event == "kr":
                    keyboard.release(int(value))
            except:
                print("err")
