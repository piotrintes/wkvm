# Wireless KVM

This software will allow you to turn a whatever-pi device into an inteligent keyboard/mouse KVM switch for any number of Windows/Linux systems.

## Features (current)

- Allow for wireless connection of any number of devices via TCP/IP
- Device switching using keyboard shortcut

## Features (planned)

- Device switching using web interface tablet/phone
- Inteligent device switching when mouse cursor reaches screen edge
- Using web interface on tablet/phone as keyboard or mouse to control your device
- Encrypting the connection to make it usable in potentially insecure networks
- USB transmitter-receiver (Arduino/ESP-32) that would work like generic mouse/keyboard, in case you don't want to (or can't) install software on the client device.
- Bluetooth (maybe one day)

## How it works

- Install WKVM server on whatever-pi device
- Connect keyboard and mouse to the device (optional once virtual web keyboard/mouse are working)
- Install WKVM client on the client device, and pair it with the WKVM server
- Set up switching methods to configure the enviroment (either using one of the client devices or via web interface)

## Supported systems

### WKVM Server
Any device running Linux, but it makes most sense with small ARM devices like raspberry pi

### WKVM Client

- Linux
- Windows (not yet)
- Android (planned in future)
- Mac (maybe one day)