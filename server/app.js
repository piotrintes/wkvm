const deviceReader = require("./components/deviceReader");

const hostname = '127.0.0.1';
const port = 3000;

var express = require('express')
var app = express()

app.get('/ver', function (req, res) {
     res.send(JSON.stringify({ version: '1.0.0' }));
})

app.get('/api/devices', function (req, res) {
     res.send(deviceReader.getDeviceList());
})

app.use('/', express.static(__dirname + "/front/"));

app.listen(port, hostname, () => {
     console.log(`Server running at http://${hostname}:${port}/`);
});