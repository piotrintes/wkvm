#!/python3
# sudo pip3 install evdev
# sudo dnf install python-evdev

import evdev
import sys

DEV = sys.argv[1]

# Set up the input device (keyboard or mouse)
device = evdev.InputDevice(DEV)
print(f"Listening to: {device}")

# Listen in loop for relevent events
for event in device.read_loop():

    # if (event.type == 2 and event.code == 1):
    #     event_data = f"type={event.type}, code={event.code}, value={event.value}"
    #     print(event_data)

    if (event.type == 3 and event.code == 0):
        print("tx:" + str(event.value))
    elif (event.type == 3 and event.code == 1):
        print("ty:" + str(event.value))
    elif (event.type == 2 and event.code == 0):
        print("mh:" + str(event.value))
    elif (event.type == 2 and event.code == 1):
        print("mv:" + str(event.value))
    elif (event.type == 2 and event.code == 11):
        print("sv:" + str(event.value))
    elif (event.type == 2 and event.code == 12):
        print("sh:" + str(event.value))
    elif (event.type == 1 and event.value == 1):
        print("kp:" + str(event.code))
    elif (event.type == 1 and event.value == 0):
        print("kr:" + str(event.code))
