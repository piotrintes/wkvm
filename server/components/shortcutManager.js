const server = require('./tcpServer')

// remember state of super-keys
let meta = false;
let ctrlL = false;
let ctrlR = false;
let altL = false;
let latR = false;
let shiftL = false;
let shiftR = false;

/** Process shortcuts from keyboard input
 * @param code input code (event:keycode)
 */
exports.processShortcut = (code) => {
     const event = code.split(':')[0]
     const keycode = code.split(':')[1]

     // set super keys
     if (keycode == 125) meta = event == 'kp';
     else if (keycode == 29) ctrlL = event == 'kp';
     else if (keycode == 97) ctrlR = event == 'kp';
     else if (keycode == 56) altL = event == 'kp';
     else if (keycode == 100) latR = event == 'kp';
     else if (keycode == 42) shiftL = event == 'kp';
     else if (keycode == 54) shiftR = event == 'kp';

     // check for shortcuts
     if (event != 'kp') return

     if (meta && keycode >= 59 && keycode <= 70) {
          server.setClientByNumber(keycode - 59);
     }
}