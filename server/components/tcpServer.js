var net = require('net');

let clients = new Map();
let keyMap = new Map();
let selectedClient = undefined;
let highestId = 0;

var server = net.createServer(function (socket) {
     // set new client id (must be unique)
     const newId = highestId++

     // register client
     clients.set(newId, {
          id: newId,
          address: socket.address().address,
          socket: socket,
          key: null
     })
     console.log('Connection with', socket.address().address, '- id assigned', newId);

     // if socket closes, remove client from registery
     socket.on('close', () => {
          console.log('Connection lost with', clients.get(newId).address, '- id assigned', newId);
          clients.delete(newId)
          try {
               keyMap.delete(clients.get(newId).key);
          } catch { }
     });

     socket.on('error', () => { });

     // execute remote commands
     socket.on('data', data => {
          data = data.toString();

          if (data[0] == 'k') {
               // set client key (self-assigned name)
               clients.get(newId).key = data.substring(1);
               keyMap.set(clients.get(newId).key, newId)
               console.log('Client', newId, 'key set to', clients.get(newId).key)
          } else if (data[0] == 's') {
               // switch output to client (selected by client key)
               setClientByKey(data.substring(1));
          }
     })

     // introduce server to the client (to confirm the connection is established)
     socket.write('WKVM server\r\n');
     socket.pipe(socket);

     // if no other client is selected, select new client
     if (selectedClient == undefined) selectedClient = newId;
});

// start TCP server
server.listen(3001);

/** Send message to the currently selected client
 * @param msg message to be sent
 */
exports.send = (msg) => {
     if (!clients.has(selectedClient)) {
          selectedClient = highestId - 1;
          return;
     }
     clients.get(selectedClient).socket.write(msg + '\r');
}

/** Get the list of currently connected clients */
exports.getClients = () => {
     let clientsJson = [];
     clients.forEach(client => {
          clientsJson.push({
               id: client.id,
               address: client.address,
               key: key
          })
     })
     return clientsJson;
}

/** Select client by id
 * @param id id of client to be selected
 */
exports.setClient = (id) => {
     console.log('Switching to client', id)
     if (clients.has(id)) selectedClient = id;
}

/** Select client by key
 * @param key key of client to be selected
 */
function setClientByKey(key) {
     if (!keyMap.has(key)) return;
     const id = keyMap.get(key);
     console.log('Switching to client', id, 'by key', key)
     if (clients.has(id)) selectedClient = id;
}

/** Select client by number
 * @param number number of client to be selected
 */
exports.setClientByNumber = (number) => {
     let currentNumber = 0;
     clients.forEach((val, id) => {
          if (currentNumber == number) {
               console.log('Switching to client', id)
               if (clients.has(id)) selectedClient = id;
          }
          currentNumber++;
     });
}