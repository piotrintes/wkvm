var exec = require('child_process').exec;
var spawn = require('child_process').spawn;

const server = require('./tcpServer')
const shortcutManager = require('./shortcutManager')

let device = new Map();
let dataProcessors = new Map();

exports.getDeviceList = function () {
     let deviceInfo = []
     device.forEach(dev => {
          deviceInfo.push({
               name: dev.name,
               type: dev.type
          });
     });

     return deviceInfo;
}

/** Updates list of device reader processes  */
async function updateDeviceReaders() {
     // read list of event devices
     exec('ls /dev/input/event*', (error, stdout) => {
          if (error !== null) console.log('exec error: ' + error);

          stdout.split('\n').forEach(path => {
               if (path == "") return;

               // get device info
               exec('udevadm info -a ' + path, (error, stdout) => {
                    if (error !== null) console.log('udevadm exec error: ' + error);

                    // get device name
                    let name = stdout.split('ATTRS{name}==')[1].split('\n')[0].replace('"', '').replace('"', '');

                    // categorize the device as mouse or keyboard
                    let deviceType = '';
                    if (name.toLowerCase().includes('mouse')) deviceType = 'mouse'
                    if (name.toLowerCase().includes('pad')) deviceType = 'trackpad'
                    if (name.toLowerCase().includes('keyboard')) deviceType = 'keyboard'

                    // if device is recognized, and it isn't being registered, add it the list and run service for it
                    if (deviceType != '' && !device.has(path)) {
                         console.log('Found', deviceType, '-', name, "at", path)

                         // start device capture process
                         let process = spawn('python3', ['-u', 'capture-service.py', path], {
                              detached: true, stdio: ['ignore', 'pipe', 'pipe']
                         });
                         process.unref();

                         // register device capture process
                         device.set(path, {
                              'type': deviceType,
                              'name': name,
                              'process': process
                         });

                         // if device capture process closes (ex. device disconnected) remove it from registery
                         process.on('close', () => {
                              console.log('Removing', name, "at", path);
                              device.delete(path)
                         });

                         // handle device capture process output
                         process.stdout.on('data', function (msg) {
                              dataProcessors.forEach(receiver => receiver(msg.toString(), deviceType));
                         });
                    }
               })
          });
     });
}

updateDeviceReaders();

setInterval(updateDeviceReaders, 5000);


// add data processors

/*dataReceivers.set('console', (data, deviceType) => {
     console.log(deviceType, data);
});*/

dataProcessors.set('tcp', (data, deviceType) => {
     server.send(`${deviceType[0]}:${data}`);
});

dataProcessors.set('shortcut', (data, deviceType) => {
     if (deviceType == 'keyboard') shortcutManager.processShortcut(data)
});